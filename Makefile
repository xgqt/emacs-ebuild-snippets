PWD         := $(shell pwd)

ELS          = $(wildcard $(PWD)/*.el)
ELCS         = $(ELS:.el=.elc)

EMACS       := emacs
RM          := rm -f
SH          := sh

EMACFLAGS   := --batch -q --no-site-file -L $(PWD)
EMACSCMD     = $(EMACS) $(EMACFLAGS)


.PHONY: all
all: clean compile

.PHONY: clean
clean:
	$(RM) $(ELCS)

%.elc:
	$(EMACSCMD) --eval "(byte-compile-file \"$(*).el\" 0)"

.PHONY: compile-snippets
compile-snippets:
	$(EMACS) --script $(PWD)/scripts/compile-snippets.el

.PHONY: compile
compile: $(ELCS)
compile: compile-snippets

.PHONY: eldev-install
eldev-install:
	$(MAKE) -B clean
	$(MAKE) -B $(ELCS)
	$(SH) $(PWD)/scripts/eldev-install.sh
