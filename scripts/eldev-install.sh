#!/bin/sh


# Copyright 2023 Gentoo Authors


# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


set -e
export PATH
trap 'exit 128' INT


root="$(dirname "${0}")/../"

dist_dir="${root}"/dist

if [ -z "${EMACS}" ] ; then
    emacs="emacs"
else
    emacs="${EMACS}"
fi


cd "${root}"

rm -r "${dist_dir}"

make all

eldev --time --verbose package

cd "${dist_dir}"

"${emacs}"                                                                  \
    --batch                                                                 \
    -q                                                                      \
    --eval "(require 'package)"                                             \
    --eval "(package-install-file \"$(find . -type f -name "*.tar" )\")"
