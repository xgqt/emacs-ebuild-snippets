#!/usr/bin/env python3


"""
Generate phase snippets
""" """
Copyright 2023 Gentoo Authors


This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
"""


# See Gentoo Developer Manual for a reference:
# https://devmanual.gentoo.org/ebuild-writing/functions/index.html#contents

EBUILD_PHASES = [
    "pkg_pretend",
    "pkg_nofetch",
    "pkg_setup",
    "src_unpack",
    "src_prepare",
    "src_configure",
    "src_compile",
    "src_test",
    "src_install",
    "pkg_preinst",
    "pkg_postinst",
    "pkg_prerm",
    "pkg_postrm",
    "pkg_config",
    "pkg_info",
]

YASNIPPET_BODY = """# -*- mode: snippet; indent-tabs-mode: t; -*-


# Copyright 2023 Gentoo Authors


# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


# name: {ebuild_phase}
# key: {ebuild_phase}


# --
{ebuild_phase}() {{
\t${{1:default}}
\t$2
}}
"""


def main():
    """Main."""

    for ebuild_phase in EBUILD_PHASES:
        yas_path = f"./snippets/ebuild-mode/{ebuild_phase}.yas"

        print(f'Generating: "{yas_path}"...')

        with open(yas_path, "w", encoding="utf-8") as yas_buffer:
            yas_buffer.write(YASNIPPET_BODY.format(ebuild_phase=ebuild_phase))


if __name__ == "__main__":
    main()
