(add-to-list 'load-path "@SITELISP@")
(autoload 'ebuild-snippets-initialize "ebuild-snippets"
  "Ebuild-Snippets initialization.")
(add-hook 'ebuild-mode-hook 'ebuild-snippets-initialize)
(add-hook 'nxml-mode-hook 'ebuild-snippets-initialize)
