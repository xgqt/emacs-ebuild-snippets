;;; ebuild-snippets.el --- Yasnippets for editing ebuilds and eclasses -*- lexical-binding: t -*-



;; Copyright 2023 Gentoo Authors


;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@gentoo.org>
;; Created: 27 Feb 2023
;; Version: 2.0.4
;; Keywords: languages
;; Homepage: https://gitweb.gentoo.org/proj/emacs-ebuild-snippets.git
;; Package-Requires: ((emacs "24"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:


;; Yasnippets for editing ebuilds and eclasses.

;; To use this you will also need the ebuild-mode package installed.

;; To enable ebuild-snippets you will have to call
;; `ebuild-snippets-initialize' somewhere in your "init.el" file, for example:
;; (eval-after-load 'yasnippet
;;  '(ebuild-snippets-initialize))



;;; Code:


(require 'yasnippet)


(defconst ebuild-snippets-version "2.0.4"
  "Ebuild-Snippets version.")


;; Customization

(defgroup ebuild-snippets nil
  "Customization for Ebuild-Snippets."
  :group 'ebuild)

(defcustom ebuild-snippets-directory
  (expand-file-name "snippets"
                    ;; This is a hack, "or" will take 1st non nil value,
                    ;; we use this to overwrite this value in system-wide
                    ;; install, see the emacs-ebuild-snippets ebuild.
                    (or nil
                        ;; changeme
                        (file-name-directory load-file-name)))
  "Location of yasnippets from Ebuild-Snippets."
  :safe 'stringp
  :type 'file
  :group 'ebuild-snippets)


;; Main provided features

;;;###autoload
(defun ebuild-snippets-initialize ()
  "Ebuild-Snippets initialization."
  (add-to-list 'yas-snippet-dirs ebuild-snippets-directory t)
  (yas-load-directory ebuild-snippets-directory))

;;;###autoload
(add-hook 'ebuild-mode-hook 'ebuild-snippets-initialize)

;;;###autoload
(add-hook 'nxml-mode-hook 'ebuild-snippets-initialize)


(provide 'ebuild-snippets)



;;; ebuild-snippets.el ends here
