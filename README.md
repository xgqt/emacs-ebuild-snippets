# Emacs-Ebuild-Snippets

Yasnippets for editing Ebuilds, Eclasses and Gentoo package metadata.

## Installation

### System

If you are on Gentoo Linux, then just emerge
`app-emacs/emacs-ebuild-snippets`.

``` shell
emerge app-emacs/emacs-ebuild-snippets
```

### User

If you want to install this package under your current user only, then you
have to use the [Eldev](https://github.com/doublep/eldev/) tool or manually
copy the `snippets` directory to the location where you installed
`ebuild-snippets.el`.

``` shell
make eldev-install
```

## Upstream

Upstream can be found on the Gentoo official GitWeb:
https://gitweb.gentoo.org/proj/emacs-ebuild-snippets.git/

## License

Copyright (c) 2023 Gentoo Authors

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
